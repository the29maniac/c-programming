#include<stdio.h>
void fc(int a){
    
}
int main(){
    int class_choice;   //gave the user a choice for the choose the type of class of airline : First class and Economy class
    int first_class[5]={0,0,0,0,0};//initialized all the eliments if the first class with 0 indicating that the seat is not booked.
    int economy[5]={0,0,0,0,0};//same as above
    int i=0,j=0,s=1;// initialised 2 variables i,j for for loops in the while loop for s=1;
    char c;//initialized a character to a variable
    while(s==1){// started an infinite loop so that the programm keeps on running
        printf("\n=============================================================================\n");//made the structure of the programm
        printf("Please type 1 for first class\n");
        printf("Please type 2 for economy:\n");
        scanf("%d",&class_choice);//gave the user a choice to make for the type of class
        if(class_choice==1){    //if the user chooses 1 ie first class then first element(as i=0) of the first class array gets changed to 1 indicating the seat to be booked.
            for(; i<=5;){   //started a fot loop for i to be less than 5 as mentioned in the q that there are only 5 seats in both the classes
                if(first_class[i]==0 && i<5){   //checked the condition if an element of first_class if 0 and is less than 5 at the same time
                    printf("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
                    printf("Congratulations Your Seat No.: %d is now booked in First Class.\n",i+1);
                    printf("Keep this pass with you while boarding.\n");//booked the ticket
                    printf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
                    first_class[i]=1;   //changed the value of the element of the first class to be 1 insted of 0 (1 indicating seat booked)
                    i++;//increased the value of i
                    break;//broken the for loop here and the program runs again from while loop until i is less than 5
                }
                else if(i>=5){  //if i becomes greater than or equal to 5 then the program will show that all the seats of first class are booked now and if the user wish to go for economy class then he can or either wait for the next flight
                    printf("\nSorry all the seats in First Class are now booked.\n");
                    printf("Do you want to book one in Economy Class?\n");
                    printf("Enter your choice y for yes and n for no.:\n");
                    scanf(" %c",&c);
                    if(c=='n'){//if the user chooses to not book a seat in economy class then the for loop breaks and again while loop starts
                        printf("\nNext flight leaves in 3 hours.\n");
                        printf("You may wait.\n");
                        break;
                    }
                    else{//if the user writes something else instead of 'n' then his ticket in economy class will be booked
                        if(j<5){ //checked if j<5 ie. if there is seat available in economy class
                            printf("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
                            printf("Congratulations Your Seat No.: %d is now booked in Economy Class.\n",j+1);
                            printf("Keep this pass with you while boarding.\n");
                            printf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
                            economy[j]=1; //changed the element of economyclass to 1
                            j++;    //increased the value of j after ticket is booked
                            break; // for loop is broken and then while loop is executed
                        }
                        else{//if all the seats in economy class are full too(+ seats in first class are full) then the program will show this
                            printf("\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n");
                            printf("\nSorry! All the seats in this flight are booked for now.\n");
                            printf("Next Flight leaves in 3 hours.\n");
                            printf("\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n");
                            break; //while loop begins again after closing this for loop here
                        }
                    }
                }
                break;
            }
        }
        else if(class_choice==2){//same logic applied as for first class
            for(; j<=5;){
                if(economy[j]==0 && j<5){
                    printf("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
                    printf("Congratulations Your Seat No.: %d is now booked in Economy Class.\n",j+1);
                    printf("Keep this pass with you while boarding.\n");
                    printf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
                    economy[j]=1;
                    j++;
                    break;
                }
                else if(j>=5){
                    printf("\nSorry all the seats in First Class are now booked.\n");
                    printf("Do you want to book one in Economy Class?\n");
                    printf("Enter your choice y for yes and n for no.:\n");
                    scanf(" %c",&c);
                    if(c=='n'){
                        printf("\nNext flight leaves in 3 hours.\n");
                        printf("You may wait.\n");
                        break;
                    }
                    else{
                        if(i<5){
                            printf("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
                            printf("Congratulations Your Seat No.: %d is now booked in First Class.\n",i+1);
                            printf("Keep this pass with you while boarding.\n");
                            printf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
                            first_class[i]=1;
                            i++;
                            break;
                        }
                        else{
                            printf("\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n");
                            printf("\nSorry! All the seats in this flight are booked for now.\n");
                            printf("Next Flight leaves in 3 hours.\n");
                            printf("\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n");
                            break;
                        }
                    }
                }
                break;
            }
        }
        else{
            printf("\n\n\nEnter a correct choice.\n\n\n");
            printf("\n\n");
        }
    }
}